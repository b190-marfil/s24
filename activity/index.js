let getCube = (num)=> num**3;
let cube = getCube(2);
console.log(`The cube of 2 is ${cube}`);

let address = [258, "Washington Ave NW", "California", 90011];
let [streetNum, streetName, stateName, zipCode ] = address;
console.log(`I live at ${streetNum} ${streetName}, ${stateName} ${zipCode}`);

let animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
};
let {name,type,weight,measurement} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}.`);

let numbers = [1,2,3,4,5];
numbers.forEach((number)=> console.log(number));

let reduceNumber = numbers.reduce((x,y)=>x+y);
console.log(reduceNumber);

class Dog{
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};
let newDog = new Dog("Frankie",5,"Miniature Dachshund");
console.log(newDog);